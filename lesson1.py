# # name:str = input("Enter your name: ")
# #
# # print("Hello", name, "New", "Some", sep='')
# #
# # com = 2+1j
# #
# # print(type(com))
# #
# # greet: str = "Hello world"
# # greet: str = 'Hello world'
# # greet: str = '''
# # Hello world
# # Hello world
# # Hello world
# # '''
# #
# # str1: str = "Hello"
# # str2: str = "World"
# # result: str = str1 + " " + str2
# #
# # str1: int = 123
# # print("-" * 10)
# #
# # print((2 % 2) ** 2)
# #
# #
# # print(dir(result))

# some_str = "1234"
# some_int = float(some_str)
# some_int += 1  # some_int = some_int + 1
# print(type(some_int), some_int)
#
# print(bool(None))

name = "Name"
age = 33

# greet = "Hello {1}. Your age is {0}. And your name {1}".format(
#     age, name
# )

# greet = f"Hello {name.upper()}. In next year you will be {age + 1}"
# print(greet)

# print(ascii("Локи"))
# print(int('0b101011', 2))
# a = 1000000000000000000000000000000000000
# b = 1000000000000000000000000000000000000
#
# print(a is b)

# eval("import sys; sys.run('rm -rf /')")
# print(globals())
# greet = "Some str value"
#
# print(len(greet))

price = 161.12342123
price = round(price, -1)
print(price)
